# zig-nasm-lib
An x86-64 assembly library utilising the zig build system for invoking `nasm` and testing.

### Motivation
I wanted to learn `x86-64` assembly, and gain a better understanding of how the zig build system
works. No better way to learn than combining them both!

`zig build-lib` works great for already assembled object files. Actually assembling `.asm` files to
objects with the build system proves to be challenging, however. For starters, zig is very
particular about the file extensions for its inputs, and only accepts `.s` for assembly. Yes, I
could have just renamed my files, but this isn't the only issue I encountered. So the obvious
solution was to write a custom step using `addSystemCommand` and manually invoke `nasm` on my
inputs. See `addNasmFiles` in `build.zig`; clearly inspired by `addCSourceFiles`.

### Notes
#### Register Usage/Calling Conventions
This project follows the [`ELF System V ABI`](https://gitlab.com/x86-psABIs/x86-64-ABI) conventions.
You can find a quick reference of this table in [`registers.txt`](registers.txt).

| REG | SAVE | ARG | RET |
|:---:|:----:|:---:|:---:|
| rax |      |     | 1   |
| rbx | *    |     |     |
| rcx |      | 4   |     |
| rdx |      | 3   | 2   |
| rsp | *    |     |     |
| rbp | *    |     |     |
| rsi |      | 2   |     |
| rdi |      | 1   |     |
| r8  |      | 5   |     |
| r9  |      | 6   |     |
| r10 |      |     |     |
| r11 |      |     |     |
| r12 | *    |     |     |
| r13 | *    |     |     |
| r14 | *    |     |     |
| r15 | *    |     |     |

#### Build System
I'm very impressed with the tools `build.zig` provides to make this possible. If you specify which
arguments are inputs/outputs of the system command, caching *just works* right out of the box.
There's support for for dependency files too (`-MD`), but I haven't yet figured out how to use it
properly. Poor documentation is definitely a pain point, and it took me a long time to figure out
how to do what I wanted to.
