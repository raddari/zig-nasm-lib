const std = @import("std");
const testing = std.testing;

extern fn strlen(str: [*]const u8) usize;

test "strlen" {
    try testing.expectEqual(0, strlen(""));
    try testing.expectEqual(5, strlen("hello"));
}
