;
; Operations involving strings.
;

section .text
default rel

global strlen

; Length of null terminated string.
;   rdi <- str
;   rax -> len
strlen:
	mov rax, rdi
.loop:
	cmp byte [rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	sub rax, rdi
	ret
