const std = @import("std");

pub fn build(b: *std.Build) void {
    const optimize = b.standardOptimizeOption(.{});
    const target = b.resolveTargetQuery(.{ .os_tag = .linux, .ofmt = .elf, .cpu_arch = .x86_64 });

    const lib_asm = b.addStaticLibrary(.{
        .name = "znl",
        .target = target,
        .optimize = optimize,
        .use_lld = false,
    });

    addNasmFiles(lib_asm, .{
        .root = b.path("src"),
        .flags = &.{
            "-felf64",
            "-Wall",
            if (optimize == .Debug) "-g" else "",
        },
        .files = &.{
            "string.asm",
        },
    });

    b.installArtifact(lib_asm);

    const lib_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/test.zig"),
        .target = b.graph.host,
        .optimize = optimize,
        .use_lld = false,
        .use_llvm = false,
    });

    lib_unit_tests.linkLibrary(lib_asm);

    const run_lib_unit_tests = b.addRunArtifact(lib_unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_lib_unit_tests.step);
}

const AddNasmFilesOptions = struct {
    root: ?std.Build.LazyPath,
    files: []const []const u8,
    flags: []const []const u8 = &.{},
};

fn addNasmFiles(compile: *std.Build.Step.Compile, options: AddNasmFilesOptions) void {
    const b = compile.step.owner;
    const root = options.root orelse b.path("");

    for (options.files) |file| {
        std.debug.assert(!std.fs.path.isAbsolute(file));
        const src_file = b.pathJoin(&.{ root.src_path.sub_path, file }); // FIXME: this seems like a hack
        const file_stem = std.mem.sliceTo(file, '.');

        const nasm = b.addSystemCommand(&.{"nasm"});
        nasm.addArgs(options.flags);
        nasm.addPrefixedDirectoryArg("-i", root);
        const obj = nasm.addPrefixedOutputFileArg("-o", b.fmt("{s}.o", .{file_stem}));
        nasm.addFileArg(b.path(src_file));

        compile.addObjectFile(obj);
    }
}
